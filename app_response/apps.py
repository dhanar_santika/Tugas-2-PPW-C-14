from django.apps import AppConfig


class AppsResponseConfig(AppConfig):
    name = 'app_response'
