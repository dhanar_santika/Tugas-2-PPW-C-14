from django.shortcuts import render
from django.http import HttpResponseRedirect


response = {}

def index(request):
	response['Comment'] = Comment
	html = 'app_response/app_response.html'
	forum = Forum.objects.all()
	response['forum'] = forum
	return render(request, html, response)
    
def add_response(request):
	form = CommentForm(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['comment'] = request.POST.get('comment')
		response['id'] = request.POST.get('data.id')
		obj = Forum.objects.get(pk=response['id'])
		comment = Comment(response['comment'])
		comment.save()
		return HttpResponseRedirect('/menanggapi/')
