# Tugas 2 PPW Kelas C Kelompok 14

Nama Anggota :

Made Wira Dhanar Santika (1606880996)

Usama (1606862766)

Zaki Ibrahim (1606836351)

Ignatius Raka (1606885555)

# Status Pipeline dan Coverage
[![pipeline status](https://gitlab.com/dhanar_santika/Tugas-2-PPW-C-14/badges/master/pipeline.svg)](https://gitlab.com/dhanar_santika/Tugas-2-PPW-C-14/commits/master)

[![coverage report](https://gitlab.com/dhanar_santika/Tugas-2-PPW-C-14/badges/master/coverage.svg)](https://gitlab.com/dhanar_santika/Tugas-2-PPW-C-14/commits/master)

# Herokuapp Link
http://bukalaptop.herokuapp.com/
