function linkedinLogin(){
	console.log('linkedinLogin called')
	var src="http://platform.linkedin.com/in.js"
	api_key: '81q5awfazmzhge'
	authorize: true
	onLoad: onLinkedInLoad
}

function onLinkedInLoad() {
	IN.Event.on(IN, "auth", getProfileData);
}

// Handle the successful return from the API call
function onSuccess(data) {
	console.log(data);
}

// Handle an error response from the API call
function onError(error) {
	console.log(error);
}

// Use the API call wrapper to request the member's basic profile data
function getProfileData() {
	IN.API.Raw("/people/~").result(onSuccess).error(onError);
}