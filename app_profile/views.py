from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
response = {}

def index(request):
    if('id' in request.session):
        try:
            response['name'] = request.session.get('name')
            response['type'] = request.session.get('type')
            if(response['name']==None and response['type']==None):
                raise KeyError
        except KeyError:
            get_data(request)

        return render(request, 'profile.html', response)
    else:
        return HttpResponseRedirect('/dashboard/')

@csrf_exempt
def get_data(request):
    request.session['name'] = request.POST.get('name')
    request.session['type'] = request.POST.get('type')
    return HttpResponseRedirect('/profile/')