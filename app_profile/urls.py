from django.conf.urls import url
from .views import index,get_data
#url for app
urlpatterns = [
    url(r'^$', index, name='profile'),
    url(r'^get_data/',get_data,name="get_data")
]
