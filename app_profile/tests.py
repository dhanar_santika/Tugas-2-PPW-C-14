from django.test import TestCase, Client
from .views import index
from django.contrib.auth.models import User

# Create your tests here.
class ProfileTest(TestCase):
    def test_index_go_to_dashboard(self):
        request = Client().get('/profile/')
        self.assertEqual(request['location'],'/dashboard/')

    def test_index_go_to_profile(self):
        session = self.client.session
        session['id'] = '123456'
        session.save()
        response = self.client.get('/profile/')
        
        session['name'] = 'BukaLaptop'
        session['type'] = 'Non Profit'
        session.save()

        request = self.client.get('/profile/')
        
    def test_get_data(self):
        request = Client().post('/profile/get_data/',data={'name':'BukaLaptop','type':'Non Profit'})
        self.assertEqual(request['location'],'/profile/')
