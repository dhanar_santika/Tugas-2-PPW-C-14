function onLinkedInLoad() {
	IN.Event.on(IN, "auth", getProfileData);
}

// Handle the successful return from the API call
function onSuccess(data) {
    $.post('get_data/', data.values[0].positions.values[0].company)
    console.log(data)
}

// Handle an error response from the API call
function onError(error) {
	console.log(error);
}

// Use the API call wrapper to request the member's basic profile data
function getProfileData() {
	IN.API.Profile("me").fields('positions:(company:(name,type))').result(onSuccess).error(onError);
}