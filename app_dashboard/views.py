from django.shortcuts import render

response = {}

def index(request):
	html = 'dashboard/dashboard.html'
	return render(request, html, response)