function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getUserCompanies);
}

function getUserCompanies() {
    IN.API.Raw("/companies?format=json&is-company-admin=true").result(getFirstCompanyDetail).error(onError); 
}

function getFirstCompanyDetail(data) {
    console.log(data)
    IN.API.Raw("/companies/" + data.values[0].id + ":(id,name,company-type,specialties,website-url,square-logo-url)?format=json").result(companyLogin).error(onError); 
}

function onSuccess(data) {
    console.log(data);
}

function onError(error) {
    console.log(error);
}